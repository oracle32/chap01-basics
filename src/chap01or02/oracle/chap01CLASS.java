package chap01or02.oracle;

public class chap01CLASS {
	
// 오라클 공부
	
	
/* 
* SQL (Structured Query Language) 표준 검색언어.

DQL ( Data Query Language) 데이터검색
SELECT


주요 데이터 타입 4가지
NUMBER 
CHARACTER
DATE
LOB

BETWEEN  		특정 범위에 포함되는지 비교
급여를 350만원보다 많이 받고 600만원보다 적게 받는 직원 이름과 급여 조회 
WHERE SALARY BETWEEN 3500000 AND 6000000;


LIKE NOT			LIKE 문자 패턴 비교 / 비교 연산자 – LIKE
[ ‘전’씨 성을 가진 직원 이름과 급여 조회]
WHERE EMP_NAME LIKE ‘전%’;
[ 7000번 대 4자리 국번의 전화번호를 사용하는 직원 전화번호 조회 ]
WHERE PHONE LIKE ‘___7_______’;
[ EMAIL ID 중 ‘_’앞자리가 3자리인 직원 조회 ]
WHERE EMAIL LIKE ‘___%’;
[ EMAIL ID 중 ‘_’앞자리가 3자리인 직원 조회 ]
WHERE EMAIL LIKE ‘___#_%’ ESCAPE ‘#’;

IS NUL / IS NOT NULL NULL 여부 비교
[ ‘이’씨 성이 아닌 직원 사번, 이름, 이메일 조회 ]
WHERE EMP_NAME NOT LIKE ‘이%’;
WHERE NOT EMP_NAME LIKE ‘이%’;



 IN 연산자 : 비교하려는 값 목록에 일치하는 값이 있는지 확인할때 
IN / NOT IN 		비교 값 목록에 포함/미포함 되는지 여부 비교 
[ 60번 부서와 90번 부서원들의 이름, 부서코드, 급여 조회]
WHERE DEPT_CODE IN (‘D6’ ,’D8’);






-- 함수(FUNCTION) : 컬럼 값을 읽어서 계산한 결과를 리턴한다.
-- 단일행(SINGLE ROW) 함수 : 컬럼에 기록된 N개의 값을 읽어서 N개의 결과를 리턴
-- 그룹(GROUP)함수 : 컬럼에 기록된 N개의 값을 읽어서 한 개의 결과를 리턴할꺼다.

-- 함수(FUNCTION) : 컬럼 값을 읽어서 계산한 결과를 리턴한다.

SUM(숫자가 기록된 컬럼명) : 합계를 구해서 리턴한다.
ex) SUM(SALARY)
AVG (숫자가 기록된 컬럼명 ) : 평균을 구해서 리턴
ex) AVG(SALARY)
MIN (컬럼명) : 컬럼에서 가장 작은 값을 리턴
ex) MIN(SALARY)
MAX (컬럼명) : 컬럼에서 가장 큰 값을 리턴
ex) MAX(HIRE_DATE)

COUNT(*) 
ex) 현재 지금 행의 갯수가 몇갠지 알려줌 NULL 포함 전체!

COUNT(DEPT_CODE)
ex) NULL을 제외한 실제값 21개 + NULL2개

COUNT(DISTINCT DEPT_CODE) 중복을 제거하고 DEPT_CODE의 갯수
ex) (DISTINCT)중복을 제거하고 DEPT_CODE의 갯수


-- 단일행(SINGLE ROW) 함수 : 컬럼에 기록된 N개의 값을 읽어서 N개의 결과를 리턴
-- LENGTH,  LENGTHB, SUBSTR,      UPPER,  LOWER,  INSTR...
--   길이 , 바이트단위 ,문자열자르기, 대문자, 소문자,  문자가어디에 포함되어있나요.

LENGTH('오라클')	// 길이를 구하라 3
LENGTHB('오라클')  // 바이트구하라 9 (한글 3byte 영어 1byte)
LENGTHB('oracle')  // 바이트구하라 6 ( 영어라서 6byte) 1씩 + 특수기호도 1byte다.


 LPAD(EMAIL, 20, '#')
-- 20글자 안되면 #으로 채운다 왼쪽부터
RPAD(EMAIL, 20, '#')
-- 20글자 안되면 #으로 채운다 오른쪽부터
LPAD(EMAIL, 10)
-- 10개까지 나머지 짜르겠다.

LTRIM / RTRIM : 주어진 컬럼이나 문자열 왼쪽/오른쪽에서 지정한 문자 혹은 문자열을 제거
SELECT LTRIM('          OHGIRAFFERS') FROM DUAL;
-- 왼쪽 공백 제거
RTRIM 은 반대로  
SELECT RTRIM('OHGIRAFFERS         ') FROM DUAL;

-- TRIM : 주어진 컬럼이나 문자열의 앞/뒤에 지정한 문자를 제거한다
SELECT TRIM ('     OHGIRAFFERS     ') FROM DUAL;

-- BOTH 추가
SELECT TRIM (BOTH 'Z' FROM 'ZZZZ12345ZZZZ') FROM DUAL;
결과값 OHGIRAFFERS 양옆에있는 Z를 다지운다.

-- SUBSTR : 컬럼이나 문자열에서 지정한 위치로부터 지정한 갯수의 문자열을 잘라서 리턴하는 함수

SELECT SUBSTR('SHOWMETHEMONEY', 5, 2) FROM DUAL;
--             12345 M부터 2   결과값 ME 

SELECT SUBSTR('SHOWMETHEMONEY', 7) FROM DUAL;
--             1234567  결과값 THEMONEY

EMP_NAME  621235-1985634
SUBSTR(EMP_NO,1,2) 생년  -> 62
SUBSTR(EMP_NO,3,2) 생월  -> 12

HIRE_DATE 90/02/06
SUBSTR(HIRE_DATE, 1, 2) 입사년도 -> 90
SUBSTR(HIRE_DATE, 4, 2) 입사월    -> 02

-- 주민번호 가 8번째가 '2' , '4'면 여자 
SUBSTR(EMP_NO, 8, 1) = '2';  

 --WHERE 절에는 단일행 함수만 사용 가능하다.

-- LOWER / UPPER / INITCAP : 대소문자를 변경해주는 함수 
LOWER('Welcom To My world') -- 소문자로 변경
UPPER('Welcom To My world') -- 대문자로 변경
INITCAP('welcom to my world' --앞 글자만 대문자

*/
	
	
	

}
